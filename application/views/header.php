<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="description" content="Bienvenido a mi pagina" />
	<meta name="author" content="Ivan" />
	
	<link rel="icon" href="<?php echo base_url();?>assets/img/favicon.PNG" type="image/png" />
	<title> Weather mood </title>

	<!-- BOOTSTRAP -->
	<link href="<?php echo base_url();?>assets/css/CSSExternos/bootstrap.min.css" rel="stylesheet">

	<!-- FONT AWESOME STYLE  -->        
	<link href="<?php echo base_url();?>assets/fontawesome/css/all.css" rel="stylesheet" />

	<!-- ESTILOS -->
	<link href="<?php echo base_url();?>assets/css/estilosGenerales.css" rel="stylesheet" />

	<link href="<?php echo base_url();?>assets/css/estilos.css" rel="stylesheet" />

</head>
<body>