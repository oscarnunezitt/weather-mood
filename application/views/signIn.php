<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="bodycontainer">
        <img src="assets/img/titulo.png" style="width: 300px; margin-top: 10px;">
          <div class="container">
            <div><h1 class="heading-primary">Regístrate</h1></div>
            <hr class="line">
            <div class="style-paragraph" style="display: inline-flex;" class="row col-lg-12">
              <p class="heading-third">¿Ya tienes una cuenta?</p>
              <p><a class="heading-third" style="color: #5ABCE2; margin-left: 5px;text-decoration: none;" href="<?php echo base_url() ?>LogIn">Inicia sesión.</a></p>
            </div>
            <form>
              <div class="row">
                <div class="form-group col-lg-12">
                  <div class="row">
                    <div class="col-lg-6">
                      <label class="text" for="inputText4">Nombre (s)</label>
                      <input type="text" class="form-control" name="fname">
                    </div>
                    <div class="col-lg-6">
                      <label class="text" for="inputText4">Apellido Paterno</label>
                      <input type="text" class="form-control" id="inputText4">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <label class="text" for="inputText4">Apellido Materno</label>
                      <input type="text" class="form-control" id="inputText4">
                    </div>
                    <div class="col-lg-6">
                      <label class="text" for="inputEmail4">Correo eléctronico</label>
                      <input type="email" class="form-control" id="inputEmail4">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <label class="text" for="inputPassword4">Contraseña</label>
                      <input type="password" class="form-control" id="inputPassword4">
                    </div>
                    <div class="col-lg-6">
                      <label class="text" for="inputPassword4">Confirmar contraseña</label>
                      <input type="password" class="form-control" id="inputPassword4">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col" style="margin-top: 20px"><button style="background: #5ABCE2; margin-top: 20px;margin:auto; display:block;" class="btn btn-success">Regístrate</button></div>       
            </form>
          </div>
      </div>