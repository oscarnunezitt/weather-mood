<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="bodycontainer">
	<img src="assets/img/titulo.png" style="width: 300px; margin-top: 10px;">
	<div class="container">
		<div><h1 class="heading-primary">Bienvenido</h1></div>
		<hr class="line">
		<div><label class="heading-third">Inicia sesión para continuar</label></div>
		<form>
			<div class="form-row">
				<div class="form-group col-lg-12">
				<label class="text" for="inputEmail4">Correo eléctronico</label>
				<input type="email" class="form-control" id="inputEmail4">
				</div>
				<div class="form-group col-lg-12">
				<label class="text" for="inputPassword4">Contraseña</label>
				<input type="password" class="form-control" id="inputPassword4">
				</div>
			</div>
			<div class="row">
				<div style="float: left;" class="col">
				<div class="checkbox">
					<label class="text">
					<input style="margin-right: 10px" type="checkbox"> Recuérdame
					</label>
				</div>
				</div>
				<div class="text" style="display: inline;" >
				<a style="float: left;">¿No tienes una cuenta?</a> 
				<a style="color: #5ABCE2; text-decoration: none;margin-left:5px;" href="<?php echo base_url() ?>SignIn">Regístrate</a>
				</div> 
			</div>
			<div style="margin-top: 20px">
				<button class="btn" style="cursor:cursor">
					<a style="color: #FAFAFA; text-decoration: none;" href="<?php echo base_url() ?>Menu">
						Iniciar sesion
					</a>
				</button>
			</div>
		</form>
	</div>
</div>

<!-- Javascript para Login -->
<script src="<?php echo base_url();?>assets/js/login.js"></script>