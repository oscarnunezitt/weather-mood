<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="bodycontainer">
	<div class="text-box">
		<img class="logotitulo" src="assets/img/titulo.png">
		<span class="heading-primary-sub">Disfruta de música selecta para ti acorde a tu día!</span>
		<div class="col" style="margin-top: 20px">
			<button class="btn" onclick="window.location = '<?php echo base_url() ?>login'">Empezar</button>
		</div>
	</div>
	<div class="col" style="margin-top: 20px">
		<img class="biglogo" src="assets/img/logo.png">
	</div>
</div>

<!-- <button onclick="obtenerCoordenadas()">Generar lista de acuerdo al clima local</button>
<ul id="playlist">
</ul> -->


<!-- Javascript para Login -->
<script src="<?php echo base_url();?>assets/js/inicio.js"></script>
<!-- javascript para obtener la posicion de usuario y el clima -->
