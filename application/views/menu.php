<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="bodycontainer2">
	<img src="assets/img/titulo.png" style="width: 300px; margin-top: 10px;">
	<div class="btn-group btn-group-toggle" data-toggle="buttons">
		<label class="btn btn-primary active">
		<input type="checkbox" checked autocomplete="off" name="genre" value="ambient">
		Ambiental</label>
		<label class="btn btn-primary active">
		<input type="checkbox" checked autocomplete="off" name="genre" value="classical">
		Clasica</label>
		<label class="btn btn-primary active">
		<input type="checkbox" checked autocomplete="off" name="genre" value="electronic">
		Electronica</label>
		<label class="btn btn-primary active">
		<input type="checkbox" checked autocomplete="off" name="genre" value="pop">
		Pop</label>
		<label class="btn btn-primary active">
		<input type="checkbox" checked autocomplete="off" name="genre" value="rock">
		Rock</label>
	</div>
	<br>
	<br>
	<button class="btn btn-primary active" style="cursor:cursor" onclick="obtenerCoordenadas()">Generar lista de acuerdo al clima local</button>

	<ul id="playlist" style="align-content:center;margin-left:auto;margin-right:auto;display:block;">
	</ul>

</div>

<script src="<?php echo base_url();?>assets/js/localizacion.js"></script>