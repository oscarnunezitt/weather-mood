<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class http {

    //Se simplifica la llamada al metodo GET, simplemente se pasa el URL a solicitar y un arreglo de los Headers (si aplica).
    public function get($url, $headers = array())
    {
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if(count($headers) > 0){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $data = curl_exec($ch);
        curl_close($ch);
        
        return $data;
    }

    public function post($url, $body, $headers)
    {
        $options['method'] = "POST";
        $options['header'] = implode("\r\n", $headers);
        $options['content'] = http_build_query($body);
        $optionsReady = array('http' => $options);

        $context = stream_context_create($optionsReady);
        $result = file_get_contents($url, false, $context);

        return $result;
    }
}