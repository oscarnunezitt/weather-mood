<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function index()
	{ 
		$this->load->helper('text');
		$this->load->helper('url');

		$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('footer');
	}
}
