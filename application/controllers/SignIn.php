<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SignIn extends CI_Controller {

	public function index()
	{ 
		$this->load->helper('text');
		$this->load->helper('url');

		$this->load->view('header');
		$this->load->view('signIn');
		$this->load->view('footer');
	}
}
