<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentacion extends CI_Controller {

	public function index()
	{ 
		$this->load->helper('text');
		$this->load->helper('url');

		$this->load->view('header');
		$this->load->view('presentacion');
		$this->load->view('footer');
	}
}
