<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Security extends CI_Controller {

    //Este metodo se encarga de devolver al cliente su token de acceso de Spotify.
	public function getApplicationToken(){
		
		$this->load->library('http');

		//Identificadores del cliente.
		$client_id = '8c1c83e7a5244c4e8281d8b75195f571'; 
		$client_secret = '2229a87b57084d2aa7fd848f389f945a';
		$secretEncoded = base64_encode($client_id . ":" . $client_secret);

		$url = "https://accounts.spotify.com/api/token";
		
		//En formato del body es un arreglo de elementos pero con nombres como index (como si fueran objetos o JSON's).
		$body = array(
			'grant_type' => 'client_credentials'
		);

		//El formato del header es un simple arreglo de los encabezados que se quieran agregar.
		$headers = array(
			"Authorization: Basic " . $secretEncoded,
			"Content-Type: application/x-www-form-urlencoded"
		);

		//Para realizar el post, solo se envia el url, body y headers creados anteriormente.
		$result = $this->http->post($url, $body, $headers);
		
		//Se especifica que lo que se devuelve es un JSON.
		header('Content-Type: application/json');
		echo $result;
	}
}
