const id_playlist = document.getElementById("playlist");
const opciones = {
	enableHighAccuracy: true,
	timeout: 5000,
	maximumAge: 0
};

function posicionObtenida(posicion) {
	let coordenadas = posicion.coords;
	const latitud = coordenadas.latitude;
	console.log(`Latitude : ${latitud}`);
	const longitud = coordenadas.longitude;
	console.log(`Longitude: ${longitud}`);
	obtenerClima(latitud, longitud);
}

function posicionFallida(error) {
	switch (error.code) {
		case error.PERMISSION_DENIED:
			console.log("El usuario denego el permiso.");
			break;
		case error.POSITION_UNAVAILABLE:
			console.log("La nformación de localización no disponible.");
			break;
		case error.TIMEOUT:
			console.log("Expiro el tiempo para obtener la información.");
			break;
		case error.UNKNOWN_ERROR:
			console.log("Error desconocido.");
			break;
	}
}

function obtenerCoordenadas() {
	navigator.geolocation.getCurrentPosition(
		posicionObtenida,
		posicionFallida,
		opciones
	);
}

function obtenerClima(latitud, longitud) {
	url = `http://www.7timer.info/bin/api.pl?lon=${longitud}&lat=${latitud}&product=civil&output=json`;
	fetch(url)
		.then(resp => resp.json())
		.then(function(data) {
			var clima = data.dataseries[0].weather;
			console.log(`clima: ${clima}`);
			crearPlaylist(clima);
		});
}

function crearPlaylist(clima) {

	let params = new Map();
	params.set("seed_genres",getGenres());
	switch (clima) {
		case "clearday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.4);
			params.set("min_danceability", 0.3);
			params.set("max_danceability", 0.8);
			params.set("min_energy", 0.3);
			params.set("max_energy", 0.7);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.4);
			params.set("min_valence", 0.5);
			params.set("max_valence", 0.8);
			break;
		case "pcloudyday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "mcloudyday":
			params.set("min_acousticness",0.4);
			params.set("max_acousticness",0.8);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.3);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.5);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.5);
			break;
		case "cloudyday":
			params.set("min_acousticness",0.6);
			params.set("max_acousticness",0.9);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.3);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.3);
			params.set("min_instumentalness", 0.3);
			params.set("max_instumentalness", 0.7);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.5);
			break;
		case "humidday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.3);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.3);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.3);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.5);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.4);
			break;
		case "lightrainday":
			params.set("min_acousticness",0.6);
			params.set("max_acousticness",0.9);
			params.set("min_danceability", 0.1);
			params.set("max_danceability", 0.5);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.3);
			params.set("max_instumentalness", 0.8);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.3);
			break;
		case "oshowerday":
			params.set("min_acousticness",0.3);
			params.set("max_acousticness",0.6);
			params.set("min_danceability", 0.3);
			params.set("max_danceability", 0.6);
			params.set("min_energy", 0.3);
			params.set("max_energy", 0.6);
			params.set("min_instumentalness", 0.3);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.6);
			break;
		case "ishowerday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",1.0);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 1.0);
			params.set("min_energy", 0.0);
			params.set("max_energy", 1.0);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 1.0);
			params.set("min_valence", 0.0);
			params.set("max_valence", 1.0);
			break;
		case "lightsnowday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "rainday":
			params.set("min_acousticness",0.3);
			params.set("max_acousticness",0.6);
			params.set("min_danceability", 0.3);
			params.set("max_danceability", 0.6);
			params.set("min_energy", 0.3);
			params.set("max_energy", 0.6);
			params.set("min_instumentalness", 0.3);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.6);
			break;
		case "snowday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "rainsnowday":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "clearnight":
			params.set("min_acousticness",0.6);
			params.set("max_acousticness",0.9);
			params.set("min_danceability", 0.1);
			params.set("max_danceability", 0.6);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.6);
			params.set("min_instumentalness", 0.3);
			params.set("max_instumentalness", 0.7);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.5);
			break;
		case "pcloudynight":
			params.set("min_acousticness",0.2);
			params.set("max_acousticness",0.6);
			params.set("min_danceability", 0.2);
			params.set("max_danceability", 0.7);
			params.set("min_energy", 0.3);
			params.set("max_energy", 0.7);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.5);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.5);
			break;
		case "mcloudynight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.4);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.4);
			params.set("max_instumentalness", 0.7);
			params.set("min_valence", 0.2);
			params.set("max_valence", 0.6);
			break;
		case "cloudynight":
			params.set("min_acousticness",0.3);
			params.set("max_acousticness",0.7);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.5);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.5);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.5);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.5);
			break;
		case "humidnight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.2);
			params.set("max_danceability", 0.6);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.5);
			params.set("min_instumentalness", 0.2);
			params.set("max_instumentalness", 0.5);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.4);
			break;
		case "lightrainnight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "oshowernight":
			params.set("min_acousticness",0.4);
			params.set("max_acousticness",0.8);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.5);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.5);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.4);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.4);
			break;
		case "ishowernight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "lightsnownight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "rainnight":
			params.set("min_acousticness",0.6);
			params.set("max_acousticness",1.0);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.3);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.3);
			params.set("min_instumentalness", 0.5);
			params.set("max_instumentalness", 1.0);
			params.set("min_valence", 0.0);
			params.set("max_valence", 0.4);
			break;
		case "snownight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		case "rainsnownight":
			params.set("min_acousticness",0.0);
			params.set("max_acousticness",0.5);
			params.set("min_danceability", 0.0);
			params.set("max_danceability", 0.4);
			params.set("min_energy", 0.0);
			params.set("max_energy", 0.4);
			params.set("min_instumentalness", 0.0);
			params.set("max_instumentalness", 0.6);
			params.set("min_valence", 0.3);
			params.set("max_valence", 0.7);
			break;
		default:
			console.log("no se encontro el clima valido");
			return;
	}
	console.log(params);
	authSpotify(params);
}

function authSpotify(params) {
	$.get('http://localhost/weather-mood/security/getApplicationToken', function(data){
		let token = data.access_token;
		console.log(token);
		listaSpotify(token, params);
	});
}

function listaSpotify(token, params) {
	console.log(`token: ${token}`);
	console.log("params: ", params);
	let url = "https://api.spotify.com/v1/recommendations?limit=10&market=ES"
	for(var [llave, valor] of params){
		url += `&${llave}=${valor}`
	}
	console.log("url : ",url)
	fetch(url, {
		headers: {
		  Accept: "application/json",
		  Authorization:
			`Bearer ${token}`,
		  "Content-Type": "application/json"
		}
	  })
		.then(resp => resp.json())
		.then(data => mostrarPlaylist(data.tracks));
}

function mostrarPlaylist(canciones){
	$('#playlist').html('');
	canciones.forEach(album => {
		let li = document.createElement("li"),
		  a = document.createElement("a"),
		  img = document.createElement("img");
		li.style = "margin-top: 15px; margin-left:100px; animation-name: moveInLeft;animation-duration: 2s;";
		img.className="img-box"
		img.style="margin-right:15px;";
		a.style="text-decoration:none;"
		a.className="style-paragraph";
		img.src = album.album.images[1].url;
		a.textContent = album.name;
		a.href=album.external_urls.spotify;
		li.appendChild(img);
		li.appendChild(a);
		id_playlist.appendChild(li);
	  });
}

function getGenres(){
	return $(':checkbox').map(function(){return this.value;}).get().join();
}
